import React, { Component } from 'react'

import { Sidebar } from './components/Sidebar'
import MainFrame from './components/MainFrame'

class App extends Component {
  constructor() {
    super()
    this.state = {
      widgetToShow: 'CssTrianglesGenerator'
    }
  }

  widgetSwitchHandler(widgetName) {
    this.setState({
      widgetToShow: widgetName
    })
  }

  render() {
    return (
      <div className='flex items-stretch mw7 center bg-gray'>
        <Sidebar widgetSwitchHandler={this.widgetSwitchHandler.bind(this)}/>
        <MainFrame widgetToShow={this.state.widgetToShow}/>
      </div>
    )
  }
}

export default App
