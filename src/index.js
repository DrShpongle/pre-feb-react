import React from 'react'
import ReactDOM from 'react-dom'

import 'tachyons'
import './assets/stylesheets/app.css'

import App from './App'

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
