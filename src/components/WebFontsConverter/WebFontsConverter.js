import React, { Component } from 'react'

class WebFontsConverter extends Component {
  render() {
    const { classnames } = this.props
    return (
      <div className={classnames}>WebFontsConverter</div>
    )
  }
}

export default WebFontsConverter