import React, { Component } from 'react'

class FlexboxPlayground extends Component {
  render() {
    const { classnames } = this.props
    return (
      <div className={classnames}>FlexboxPlayground</div>
    )
  }
}

export default FlexboxPlayground