import React, { Component } from 'react'

import { CssTrianglesGenerator } from './CssTrianglesGenerator'
import { Base64Encoder } from './Base64Encoder'
import { FlexboxPlayground } from './FlexboxPlayground'
import { WebFontsConverter } from './WebFontsConverter'
import { ImagesOptimizer } from './ImagesOptimizer'

class MainFrame extends Component {

  render() {
    const { widgetToShow } = this.props
    const widgetCommonClasses = 'absolute top-1 right-1 bottom-1 left-1 bg-red'

    const CssTrianglesGeneratorClassnames = `${widgetCommonClasses} ${widgetToShow === 'CssTrianglesGenerator' ? '' : 'hidden'}`
    const Base64EncoderClassnames = `${widgetCommonClasses} ${widgetToShow === 'Base64Encoder' ? '' : 'hidden'}`
    const FlexboxPlaygroundClassnames = `${widgetCommonClasses} ${widgetToShow === 'FlexboxPlayground' ? '' : 'hidden'}`
    const WebFontsConverterClassnames = `${widgetCommonClasses} ${widgetToShow === 'WebFontsConverter' ? '' : 'hidden'}`
    const ImagesOptimizerClassnames = `${widgetCommonClasses} ${widgetToShow === 'ImagesOptimizer' ? '' : 'hidden'}`

    return (
      <div className='ba b--black relative w-100'>
        <CssTrianglesGenerator classnames={CssTrianglesGeneratorClassnames}/>
        <Base64Encoder classnames={Base64EncoderClassnames}/>
        <FlexboxPlayground classnames={FlexboxPlaygroundClassnames}/>
        <WebFontsConverter classnames={WebFontsConverterClassnames}/>
        <ImagesOptimizer classnames={ImagesOptimizerClassnames}/>
      </div>
    )
  }
}

export default MainFrame