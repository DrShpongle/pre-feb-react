import LogoCssTrianglesGenerator from '../../assets/images/widget-logos/logo-css-triangles-generator.png'
import LogoBase64Encoder         from '../../assets/images/widget-logos/logo-base64-encoder.png'
import LogoFlexboxPlayground     from '../../assets/images/widget-logos/logo-flexbox-playground.png'
import LogoWebFontsConverter     from '../../assets/images/widget-logos/logo-web-fonts-converter.png'
import LogoImagesOptimizer       from '../../assets/images/widget-logos/logo-images-optimizer.png'

export const widgetsData = [
  {
    widgetName:      'CSS Triangles Generator',
    widgetComponent: 'CssTrianglesGenerator',
    widgetLogo:      LogoCssTrianglesGenerator
  },
  {
    widgetName:      'BASE64 Encoder',
    widgetComponent: 'Base64Encoder',
    widgetLogo:      LogoBase64Encoder
  },
  {
    widgetName:      'Flexbox Playground',
    widgetComponent: 'FlexboxPlayground',
    widgetLogo:      LogoFlexboxPlayground
  },
  {
    widgetName:      'Web Fonts Converter',
    widgetComponent: 'WebFontsConverter',
    widgetLogo:      LogoWebFontsConverter
  },
  {
    widgetName:      'Images Optimizer',
    widgetComponent: 'ImagesOptimizer',
    widgetLogo:      LogoImagesOptimizer
  }
]