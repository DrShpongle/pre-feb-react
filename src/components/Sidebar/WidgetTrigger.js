import React from 'react'

const WidgetTrigger = (props) => {
  const { widgetName, widgetLogo, widgetSwitchHandler } = props
  return (
    <div onClick={widgetSwitchHandler} className='pointer'>
      <img src={widgetLogo}
           alt={widgetName}
           className='w3 db'/>
    </div>
  )
}

export default WidgetTrigger