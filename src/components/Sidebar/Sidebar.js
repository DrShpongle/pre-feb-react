import React, { Component } from 'react'

import { widgetsData } from './widgetsData'
import WidgetTrigger from './WidgetTrigger'

class Sidebar extends Component {
  render() {
    const { widgetSwitchHandler } = this.props
    const widgetsTriggersList = widgetsData.map((widget, index) => {
      return (
        <li key={index} className='mv3'>
          <WidgetTrigger widgetName={widget.widgetName}
                         widgetLogo={widget.widgetLogo}
                         widgetSwitchHandler={widgetSwitchHandler.bind(this, widget.widgetComponent)}/>
        </li>
      )
    })
    return (
      <div className='ph3'>
        <ul className='list pa0 ma0'>
          {widgetsTriggersList}
        </ul>
      </div>
    )
  }
}

export default Sidebar