import React, { Component } from 'react'

class ImagesOptimizer extends Component {
  render() {
    const { classnames } = this.props
    return (
      <div className={classnames}>ImagesOptimizer</div>
    )
  }
}

export default ImagesOptimizer